var gulp        = require('gulp')
var stylus      = require('gulp-stylus')
var plumber     = require('gulp-plumber')
var jade        = require('gulp-jade')
var browserSync = require('browser-sync').create();

gulp.task('stylus', function(){
    return gulp.src('src/stylus/main.styl')
        .pipe(plumber())
        .pipe(stylus())
        .pipe(plumber.stop())
        .pipe(gulp.dest('./dist/css'))
})

gulp.task('jade', function() {
  gulp.src('./src/jade/**/*.jade')
    .pipe(plumber())
    .pipe(jade({
        pretty: true
    }))
    .pipe(plumber.stop())
    .pipe(gulp.dest('./'))
})

gulp.task('watch', function(){
    gulp.watch('src/stylus/**/*.styl', ['stylus']);
    gulp.watch('src/jade/**/*.jade', ['jade']);
})

gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
})

gulp.task('build', ['jade', 'stylus', 'watch'])